#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#   Copyright (C) 2019 Christoph Fink, University of Helsinki
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 3
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, see <http://www.gnu.org/licenses/>.

"""
Rewrite the __version__ of Python modules.

Searches in current directory and rewrites __init__.py.
"""

import os
import re
import sys

import dunamai


try:
    package_name = os.environ["PACKAGE_NAME"]
    version = dunamai.Version.from_any_vcs()
    version = version.serialize(bump=(version.distance > 0))

    with open("{}/__init__.py".format(package_name), "r") as f:
        init_file = f.read()

    init_file = re.sub(
        '__version__ = "[^"]*"',
        '__version__ = "{}"'.format(version),
        init_file,
        re.MULTILINE
    )

    with open("{}/__init__.py".format(package_name), "w") as f:
        f.write(init_file)

except Exception as exception:  # pylint: disable=broad-except
    print("Rewriting __version__ failed: ", exception, file=sys.stderr)
